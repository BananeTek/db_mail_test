<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = array();
	    for ($i = 0; $i < 20000; $i++) {
		    $data []= [
			    'name' => Str::random(),
		    ];
	    }

	    Tag::insert($data);
    }
}
