<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaiSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$fai_list = array(
			['fai_id' => 1, 'name' => 'orange', 'ndd' => "orange.fr"],
			['fai_id' => 1, 'name' => 'orange', 'ndd' => "wanadoo.fr"],
			['fai_id' => 1, 'name' => 'orange', 'ndd' => "voila.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "free.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "freesbee.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "libertysurf.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "worldonline.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "online.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "alicepr.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "alicead],.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "alicemail.fr"],
			['fai_id' => 2, 'name' => 'free', 'ndd' => "infonie.fr"],
			['fai_id' => 3, 'name' => 'gmail', 'ndd' => "gmail.com"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "sfr.fr"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "neuf.fr"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "cegetel.net"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "club-internet.fr"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "numericable.fr"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "numericabl.com"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "no],.fr"],
			['fai_id' => 4, 'name' => 'sfr', 'ndd' => "neufcegetel.fr"],
			['fai_id' => 5, 'name' => 'lapost', 'ndd' => "laposte.net"],
			['fai_id' => 5, 'name' => 'lapost', 'ndd' => "laposte.fr"],
			['fai_id' => 6, 'name' => 'autre', 'ndd' => "blabla.com"],
			['fai_id' => 7, 'name' => 'hotmail', 'ndd' => "hotmail.fr"],
			['fai_id' => 7, 'name' => 'hotmail', 'ndd' => "msn.fr"],
			['fai_id' => 7, 'name' => 'hotmail', 'ndd' => "live.fr"],
			['fai_id' => 7, 'name' => 'hotmail', 'ndd' => "outlook.fr"],
			['fai_id' => 7, 'name' => 'hotmail', 'ndd' => "outlook.com"],
			['fai_id' => 7, 'name' => 'hotmail', 'ndd' => "msn.com"],
			['fai_id' => 8, 'name' => 'yahoo', 'ndd' => "yahoo.fr"],
			['fai_id' => 8, 'name' => 'yahoo', 'ndd' => "yahoo.com"],
			['fai_id' => 9, 'name' => 'aol', 'ndd' => "aol.com"],
			['fai_id' => 9, 'name' => 'aol', 'ndd' => "aol.fr"],
			['fai_id' => 10, 'name' => 'bbox', 'ndd' => "bbox.fr"],
			['fai_id' => 11, 'name' => 'apple', 'ndd' => "icloud.com"],
			['fai_id' => 12, 'name' => 'autrefr', 'ndd' => "autrefr"],
			['fai_id' => 13, 'name' => 'suisse', 'ndd' => "adressesuisse"],
			['fai_id' => 14, 'name' => 'belgique', 'ndd' => "adressebelgique"],
			['fai_id' => 15, 'name' => 'tiscali', 'ndd' => "tiscali.fr"],
		);
		DB::table('fais')->insert($fai_list);
	}
}
