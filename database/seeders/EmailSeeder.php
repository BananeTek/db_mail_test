<?php

namespace Database\Seeders;

use App\Models\Email;
use App\Models\Fai;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($size = 20000000)
    {
	    \DB::unsetEventDispatcher();
	    \DB::disableQueryLog();

	    $faiList = Fai::all();
    	$nb_ndd = $faiList->count();
	    $email_count = Email::all()->count();

	    $now = date("Y-m-d H:i:s");
	    while ($email_count < $size) {
		    $data = array();

		    for ($i = 0; $i < 20000; $i++) {
			    $idx = rand() % $nb_ndd;
			    $data [] = [
				    'email' => Str::random() . "@" . $faiList[$idx]->ndd,
				    'fai_id' => $faiList[$idx]->id,
				    'last_used' => $now,
			    ];
		    }
		    $email_count += count($data);
		    Email::insert($data);
		    unset($data);
		    $percent = ($email_count / $size) * 100;
		    echo "$email_count / $size ($percent %)" . PHP_EOL ;
		    echo "used memory = " . memory_get_peak_usage(true). PHP_EOL;
	    }
    }
}
