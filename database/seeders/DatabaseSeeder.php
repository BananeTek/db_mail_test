<?php

namespace Database\Seeders;

use App\Models\Email;
use App\Models\EmailTag;
use App\Models\Fai;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	    \DB::unsetEventDispatcher();
	    \DB::disableQueryLog();

	    if (Fai::count() === 0)
	        $this->call([FaiSeeder::class]);
	    if (Email::count() === 0)
		    $this->call([EmailSeeder::class]);
	    if (Tag::count() === 0)
		    $this->call([TagSeeder::class]);
	    if (EmailTag::count() === 0)
		    $this->call([EmailTagSeeder::class]);
    }
}
