<?php

namespace Database\Seeders;

use App\Models\Email;
use App\Models\EmailTag;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class EmailTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($size = 50000000)
    {
	    \DB::unsetEventDispatcher();
	    \DB::disableQueryLog();

	    $tag_count = Tag::count();
	    $email_count = Email::count();
	    $email_tag_count = EmailTag::count();

        while ($email_tag_count < $size) {
	        $dataset = array();

	        for ($i = 0; $i < 20000; $i++) {
		        $dataset [] = [
			        'email_id' => rand() % $email_count + 1,
			        'tag_id' => rand() % $tag_count + 1
		        ];
	        }
	        $email_tag_count += count($dataset);
	        EmailTag::insert($dataset);
	        unset($dataset);
	        $percent = ($email_tag_count / $size) * 100;
	        echo "$email_tag_count / $size ($percent %)" . PHP_EOL;
	        echo "used memory = " . memory_get_peak_usage(true). PHP_EOL;
        }
    }
}
