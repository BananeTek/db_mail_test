<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @returns Builder
 * @method static where(string $string, array|string|null $tag_id)
 */
class EmailTag extends Model
{
	protected $table = 'email_tag';

    use HasFactory;

    public function email()
    {
    	return $this->belongsTo(Email::class);
    }
}
