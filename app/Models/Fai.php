<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fai extends Model
{
    use HasFactory;

    public static function getAllNDD()
    {
    	$fais = Fai::all();
    	$nddList = array();
    	foreach ($fais as $fai) {
		    $tmp_ndd = $fai->ndd_list;
		    $tmp_ndd = explode('|', $tmp_ndd);
		    $nddList = array_merge($nddList, $tmp_ndd);
	    }
    	return $nddList;
    }
}
