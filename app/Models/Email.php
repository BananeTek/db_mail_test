<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

	public function tags()
	{
		return $this->belongsToMany(Tag::class);
	}

	public function filterByTagId($tag_id) {
		return $this->belongsToMany(Tag::class)
			->wherePivot('tag_id', $tag_id)->get();
	}
}
