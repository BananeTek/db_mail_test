<?php

namespace App\Console\Commands;

use App\Helpers\Timer;
use App\Models\Email;
use Illuminate\Console\Command;

class MassAssignTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:massAssign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$timer = new Timer();
	    $now = date("Y-m-d H:i:s");
	    $emails = Email::whereIn('id', [10, 11, 23, 456, 657, 3, 567])->update(['last_used' => $now]);
	    var_dump($emails);
        return 0;
    }
}
