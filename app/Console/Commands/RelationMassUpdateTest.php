<?php

namespace App\Console\Commands;

use App\Helpers\Timer;
use App\Models\Email;
use App\Models\EmailTag;
use App\Models\Fai;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class RelationMassUpdateTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:rel_massUpdate {tag_id} {filepath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update date for all emails having designated tag';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$timer = new Timer('test:rel_massUpdate');
    	$tag_id = $this->argument('tag_id');
    	$filepath = $this->argument('filepath');
    	$fh = fopen($filepath, 'r');
    	$emails = array();
    	while ($line = fgets($fh)) {
		    $emails []= trim($line);
	    }
	    EmailTag::where('tag_id', $tag_id)
		    ->whereHas('email', function (Builder $query) use ($emails) {
		    })
		    ->chunk(2000, function ($emailTagList) {
		    	echo count($emailTagList) . PHP_EOL;
		    });
//    	echo "test finished in : " . $timer->elapsedStr() . PHP_EOL;
	    return 0;
    }
}
