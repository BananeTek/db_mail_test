<?php

namespace App\Console\Commands;

use App\Helpers\Timer;
use App\Models\Email;
use App\Models\EmailTag;
use App\Models\User;
use Illuminate\Console\Command;

class TestRelations extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'test:relations';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		$tag_id = 256;
		$tag_ids = [256, 18, 2900, 353, 243, 6754, 2344, 2345, 10548];
		$timer = new Timer();

		$count = 0;
		file_put_contents('tmp.csv', "");
		$emails = Email::whereHas('tags', function ($q) use ($tag_id, $tag_ids, &$count) {
			$q->whereIn('tag_id', $tag_ids);
//			$q->where('tag_id', $tag_id);
		})->chunk(10000, function ($emails) {
			$filename = "tmp.csv";

			$data = array();
			foreach ($emails as $email) {
				$data []= $email->email;
			}
			$data = implode("\n", $data);
			file_put_contents($filename, $data, FILE_APPEND);

			var_dump($emails->count());
			echo "used memory = " . memory_get_peak_usage(true). PHP_EOL;
		});
		echo "total = $count" . PHP_EOL;

		return 0;
	}
}
