<?php


namespace App\Helpers;


class Timer
{

	/**
	 * @var array
	 */
	private $start_time;

	/**
	 * @var string
	 */
	private $timerName;


	public function __construct(string $timerName = '')
	{
		$this->start_time = hrtime();
		$this->timerName = $timerName;
	}

	public function __destruct()
	{
		echo $this->timerName . ' finished in : '
			. $this->elapsedStr() . ' seconds' . PHP_EOL;
	}

	/**
	 * @return array
	 */
	public function elapsed()
	{
		$now = hrtime();
		return [$now[0] - $this->start_time[0],
			$now[1] > $this->start_time[1] ?
				$now[1] - $this->start_time[1] :
				$this->start_time[1] - $now[1]
		];
	}

	/**
	 * @return string
	 */
	public function elapsedStr()
	{
		$elapsed = $this->elapsed();
		return strval($elapsed[0]) . '.' . strval($elapsed[1]);
	}

	/**
	 * @return array
	 */
	public function getStartTime()
	{
		return $this->start_time;
	}

	/**
	 * @return string
	 */
	public function getStartTimeStr()
	{
		return strval($this->start_time[0]) . '.' . strval($this->start_time[1]);
	}
}